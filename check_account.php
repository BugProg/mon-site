<?php
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=mon_site;charset=utf8', 'mon_site_user', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}

//Récupération des ids depuis l'url.
$pseudo = $_GET ['pseudo'];
$id_link = $_GET ['id_link'];


//Récupération du pseudo et de link_id
$req = $bdd->prepare('SELECT id,id_link,pseudo,enable FROM users WHERE id_link = :id_link');
$req->execute(array(
  'id_link' => $id_link));
  $resultat = $req->fetch();
  $req->closeCursor();

  //Si le lien correspond au pseudo et qu'il est valide.
  if ($pseudo == $resultat['pseudo'] AND $id_link == $resultat['id_link'] AND $id_link != '' AND $resultat['enable'] == 0)
  {//On active le compte
    $req = $bdd->prepare('UPDATE users SET enable=:enable  WHERE id_link = :id_link');
    $req->execute(array(
      'enable' => 1,
      'id_link'=> $id_link
    ));

    //On supprime l'id link
    $req = $bdd->prepare('UPDATE users SET id_link=:id_link  WHERE  = id:id');
    $req->execute(array(
      'id' => $resultat['id'],
      'id_link'=> NULL
    ));

    //Puis renvoie vers une page de confirmation
    $message_titre = "Votre compte est activé";
    $message_texte = "Vous pouvez maintenant vous connecter";
    header('Location: /message.php?titre='.$message_titre.'&message='.$message_texte.'.'); //Redirection vers la page d'acceuil
  }

  else {
    echo "non";
    //Renvoi vers une page d'erreur
    $message_titre = "ERREUR :(";
    $message_texte = "Impossible de traiter votre requête, veuillez vérifier le lien.";
    header('Location: /message.php?titre='.$message_titre.'&message='.$message_texte.'.'); //Redirection vers la page d'acceuil
  }
  ?>

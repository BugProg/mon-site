<?php
session_start(); //Ouverture de session
include ("config/config.php");
if ($_SESSION['id'] != '')//L'utilisateur est déjà connecté, donc on le renvoie vers index.php
{
  header('Location: /index.php'); //Redirection vers la page d'acceuil
  exit();
}
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr" style=" width:100%;">
<head>
  <link href="https://fonts.googleapis.com/css?family=Oswald|Sriracha&display=swap" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/waltograph" type="text/css"/>
  <link rel="stylesheet" href="css/connexion.css">
  <meta charset="utf-8">
  <title></title>
</head>
<?php

// Récupération des infos du formulaire
$email = htmlspecialchars($_POST['email']);
$password = htmlspecialchars($_POST['password']);

$_POST = array();//efface les entrées du formulaire dans php

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=mon_site;charset=utf8', 'mon_site_user', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}

?>
<body style=" width:100%;">
  <table style="padding:0; margin:0; width:100%;border-collapse: collapse;">
    <tr>
      <td style="background-color:#0254D9; height:100vh;">
        <img src="img/bulle.png" height="100%" width="auto" style="position:absolute; top:0;">
        <img src="img/degrad-verticale.png" height="50%" width="40px" style="padding:0 0 0 95%">
      </td>

      <td style="width:60%; margin-left:400px;">
        <h1 style="font-family:'WaltographRegular'; width:40px; font-weight:normal; font-size:500%; font-style:normal;position:relative; top:20px; right:0px; left:40%;">connexion</h1>
        <form action="connexion.php" method="post">
          <div>
            <label for="email"></label>
            <input type="email" id="email" name="email"required placeholder="Votre identifiant">
          </div>

          <div>
            <label for="pwd"></label>
            <input type="password" id="pwd" name="password"required placeholder="Mot de passe (Google)"></input>
          </div>
          <div class="button">
            <button onclick="myFunction()" type="submit">Se connecter</button>
            <!--    <script>
            function myFunction() {
            window.alert("Votre message à bien été envoyé");
          }
        </script>-->
      </div>
      <?php

      //  Récupération de l'utilisateur, de l'id et de son pass hashé
      $req = $bdd->prepare('SELECT id,pseudo,password,enable FROM users WHERE email = :email');
      $req->execute(array(
        'email' => $email));
        $resultat = $req->fetch();
        $req->closeCursor();

        // Comparaison du pass envoyé via le formulaire avec la base de donnée
        if (password_verify($password, $resultat['password']))
        {
          if($resultat['enable'] == 1)
          {
            $_SESSION['pseudo'] = $resultat['pseudo'];
            $_SESSION['id'] = $resultat['id'];
            header('Location: /index.php'); //Redirection vers la page d'acceuil
            exit();
          }
          else
          echo '<p style="color:red; text-align:center;" >Votre compte est désactivé</p>';

        }
        elseif ($email !='') {
          echo '<p style="color:red; text-align:center;" >Identifiant ou mot de passe erroné</p>';
        }

        ?>
      </form>
      <a href="inscription.php"> <h6>Pas encore de compte ?</h6> </a>
    </td>
  </tr>
</table>
</body>
</html>





<!--
<form action="connexion.php" method="post">
<div>
<label for="user">Login</label>
<input type="text" id="user" name="user_name"required placeholder="Votre identifiant">
</div>

<div>
<label for="pwd">Mot de passe :</label>
<input type="password" id="pwd" name="password"required placeholder="Mot de passe"></input>
</div>
<div class="button">
<button onclick="myFunction()" type="submit">Se connecter</button>
<-    <script>
function myFunction() {
window.alert("Votre message à bien été envoyé");
}
</script>->
</div>
</form>
<a href="inscription.html"> <h6>Pas encore de compte ?</h6> </a> -->

<?php session_start(); //Ouverture de session
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=mon_site;charset=utf8', $db_user, $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}

//  Récupération de message à afficher dans la PopUp
$req = $bdd->prepare('SELECT message FROM popup');
$req->execute();
$resultat = $req->fetch();
$req->closeCursor();
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald|Sriracha&display=swap" rel="stylesheet"> <!-- Importe les polices-->
  <meta charset="utf-8">
  <title>Home</title> <!-- Titre onglet-->
</head>

<body>
  <!-- header  btn -->
  <ul>
    <li><a href="https://www.home-cloud.fr" title="Cloud"> Nextcloud</a></li>
    <li><a href="https://ospcar.github.io/" title"O.S.P.Car">O.S.PCar</a></li>
    <li><a href="https://nantes-services.github.io/accueil/"> <title>Nantes Services</title>Nantes Services</a> </li>
    <li><a href="contact.html">Contacts</a></li>
  </ul>
  <?php
  if ($_SESSION['pseudo'] !='')
  {
    $name = "data/avatars/{$_SESSION['id']}"; //Nom et répertoire du fichier
    if(file_exists ($name))//On affiche l'avatar
    echo '<img src="'.$name.'" style="width:30px; position:relative; top:7px; float:right; height:auto; border: 1px solid black; border-radius: 100%;" alt=""/>';

    echo '<a style="float:right;"href="settings.php">Bonjour '.$_SESSION['pseudo'].'</a>';
  }
  else
  {
    echo '<a style="float:right;"href="connexion.php">Connexion</a>';
  }
  ?>
  <img src="img/profil.jpg" alt="BugProg" class="image_profil">
  <p class="main_paragraph" > <strong>Bienvenue</strong> sur mon site <i>web</i> !<br>Retrouvez le projet <strong><u>O.S.PCar</u></strong> et suivez en direct l'avencement du projet d'une petite voiture.</p>

  <?php // Popup // Affiche les message définis par l'administrateur.
  if ($resultat['message'] != '')
  {
    echo '
    <div id="Popup" style="display:block;">
    <img style="float:left; padding:0 0 20px 10px;" src="img/warning.png" alt="">
    <p style="float:left" id="message">'.$resultat['message'].'</p>

    <button style="float:right;font-size: 150%;  margin-top:20px; margin-right:20px;" type="button" name="button" id="button" onclick="hide()">Valider</button>
    </div>';
  }
  ?>

  <script type="text/javascript" src="js/popup.js"></script> <!--Importe le fichier.js pour la popup-->
</body>
</html>

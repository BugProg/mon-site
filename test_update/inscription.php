<?php session_start(); //Ouverture de session
include ("config/config.php");
include ("traitement_formulaire.php");
if ($_SESSION['id'] != '')//L'utilisateur est déjà connecté, donc on le renvoie vers index.php
{
  header('Location: /index.php'); //Redirection vers la page d'acceuil
  exit();
}
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr" style=" width:100%;">
<head>
  <link href="https://fonts.googleapis.com/css?family=Oswald|Sriracha&display=swap" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/waltograph" type="text/css"/>
  <link rel="stylesheet" href="css/connexion.css">
  <meta charset="utf-8">
  <title>inscription</title>
</head>
<?php

// Récupération des infos du formulaire
$pseudo = htmlspecialchars($_POST['pseudo']);
$email = htmlspecialchars($_POST['email']);
$password = htmlspecialchars($_POST['password']);
$password_confirm = htmlspecialchars($_POST['confirm_password']);

$_POST = array();//formate le formulaire

// Hash du mot de passe
$password_hash = password_hash($password, PASSWORD_BCRYPT);

try // tentative de connection à la base de données
{
    $bdd = new PDO('mysql:host='.$db_host.';dbname=mon_site;charset=utf8', $db_user, $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}

?>
<body style=" width:100%;">
  <table style="padding:0; margin:0; width:100%;border-collapse: collapse;">
    <tr>
      <td style="background-color:#0254D9; height:100vh;">
        <img src="img/bulle.png" height="100%" width="auto" style="position:absolute; top:0;">
        <img src="img/degrad-verticale.png" height="50%" width="40px" style="padding:0 0 0 95%">
      </td>

      <td style="width:60%; margin-left:400px;">
        <h1 style="font-family:'WaltographRegular'; width:40px; font-weight:normal; font-size:500%; font-style:normal;position:relative; top:20px; right:0px; left:40%;">Inscription</h1>
        <form action="inscription.php" method="post">
          <div>
            <label for="pseudo"></label>
            <input type="text" id="pseudo" name="pseudo"required placeholder="Votre pseuso">
          </div>
          <div>
            <label for="mail"></label>
            <input type="email" id="mail" name="email"required placeholder="Votre email">
          </div>

          <div>
            <label for="pwd"></label>
            <input type="password" id="pwd" name="password"required placeholder="Mot de passe (Google)"></input>
          </div>
          <div>
            <label for="pwd_confirm"></label>
            <input type="password" id="pwd_confirm" name="confirm_password"required placeholder="Confirmez votre mot de passe"></input>
          </div>
          <div class="button">
            <button onclick="myFunction()" type="submit">S'inscrire</button>
            <!--    <script>
            function myFunction() {
            window.alert("Votre message à bien été envoyé");
          }
        </script>-->
      </div>
      <?php
      if (!password_verify($password_confirm, $password_hash)) {//Vérifie si les deux mot de passe sont les même
        echo '<p style="color:red; text-align:center;" >Les mots de passes ne correspondent pas.</p>';
        $password_check=false;
      }
      else {
        $password_check=true;
      }
      $go = $bdd->query('SELECT email FROM users'); // Prend les données dans la base
      if ($email != '')
      {
        while ($data = $go->fetch())//organise les données
        {
          if ($data['email']==$email) //Si l'email existe déjà
          {
            echo '<p style="color:red; text-align:center;" >Le compte éxiste déjà</p>';
            $email_check=false;
            break;
          }
          else {
            $email_check=true;
          }
        }
      }
      $go->closeCursor();

      if ($password_check == true AND $email_check == true)
      {
        $id_link = rand (100000, 100000000);//Nombre unique pour la vérification du mail

        $req = $bdd->prepare('INSERT INTO users (pseudo,email,password, id_link) VALUES(:pseudo, :email, :password, :id_link)');
        $req->execute(array(
          'pseudo' => $pseudo,
          'email' => $email,
          'password' => $password_hash,
          'id_link' => $id_link,
        ));

        //Préparation à l'envoie d'un mail de confirmation

        //Contenue de l'email.
        $body = 'Veuillez confirmer votre compte : https://site.home-cloud.fr/check_account.php?id_link='.$id_link.'&pseudo='.$pseudo.'';

        //Envoi du mail
        envoie_mail ($body,$email,"site.home-cloud.fr");

        //Renvoie vers une page de confirmation
        $message_titre = "Confirmez votre email";
        $message_texte = "Pour continuer à utiliser votre compte veuillez confirmer votre email.";
        header('Location: /message.php?titre='.$message_titre.'&message='.$message_texte.'&next_locate=connexion.php');
        exit();
      }
      ?>
    </form>
    <a href="connexion.php"> <h6>Se connecter</h6> </a>
  </td>
</tr>
</table>
</body>
</html>

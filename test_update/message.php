<!--//Cette page permet d'afficher des messages à l'utilisateur comme l'acces refusé à une ressource ou une confirmation...-->


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link href="https://fonts.googleapis.com/css?family=Oswald|Sriracha&display=swap" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/waltograph" type="text/css"/>
</head>
  <?php

  echo '
  <a class="retour"href='.$_GET['next_locate'].'>Retour</a>
  <div class="box">
  <h1>'.$_GET['titre'].'</h1>
  <p>'.$_GET['message'].'</p>
  </div>
  ';

  echo '<style type="text/css">
  html {
    font-family: "Oswald", sans-serif;
    background: linear-gradient(70deg, #C50BF3,#0B83F3);
    height: 100%;
  }
  body
  {
    margin: 0;
    background-repeat: no-repeat;

  }

  .retour {
    float: right;
    background-color:none;
    text-decoration:none;
    text-align:center;
    color:white;
    padding: 7px 7px 7px 7px;
    display: block;
    border: 2px solid transparent;
  }
  .retour:hover {
    background-color:none;
    display: block;
    border-radius: 10px;
    color:white;
    border: 2px solid;
    text-align:center;
  }

  .box
  {
    border:4px solid white;
    border-radius: 20px;
    text-align: center;
    margin: 30vh;
  }
  div > h1
  {
    color: white;
    font-family:\'WaltographRegular\';
    font-size: 10vh;
  }

  div > p
  {
    color: white;
    font-family: \'Oswald\', sans-serif;
  }
  </style>';

  ?>

</html>

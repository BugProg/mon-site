# Documentation Administrateur

## Site web personnelle (site.home-cloud.fr)

#### Licence : GPL-3

Dernière modification : 27 Avril 2020

### Informations général

Serveur utilisé : apache (tor)

Base de donnée : Mariadb

OS : Debian Buster

### Modules utilisés

* php
* apache
* phpmailer
* composer
* php-mysql

## PHP MAILER

Php MAILER permet de faire du mailling via php.

Importation de la blibliothèques "phpmailer" dans le répertoire du site web.

`sudo composer require phpmailer/phpmailer`

---

## MYSQL

Tables présentes à ce jour:

- users
  
  - id(user)
  
  - pseudo
  
  - email
  
  - password
  
  - phone_number
  
  - bio
  
  - website

- popup
  
  - message

### Permissions

Afin de garantire la sécurité des utlisateurs et du server, des permissions restrictive doivent-être appliqué sur les dossier data(contenue des données des utlisateurs) ainsi que config (dossier ou son contenue des infos sensible comme les mot de passe de base de données)

#### Pour le dossier data :

`sudo chmod 300 /var/www/`

Apache doit aussi être le propriétaire du dossier donc :

`sudo chown -R www-data data/`

#### Pour le dossier config

`chmod 011 config/*`

apache doit aussi etre le propriétaire du dossier donc :

`chown -R www-data:www-data config/*`

## Update Website

Afin de faciliter la mise à niveau du site web le scrypt bash 'update_website.sh' met à jour les fichiers et les permissions de manière automatique.

Pour cela, mettre les nouveaux fichiers dans le répèrtoire du site sous le nom de 'site.zip'. exécuter le programme de mise à jour et c'est tout.

# Problèmes connus

> Impossible de créer le premier utilisateur.

Pour la création de premier utilisateur la base de donnée ne doit être vide.
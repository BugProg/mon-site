<?php session_start(); //Ouverture de session
include ("config/config.php");
if ($_SESSION['id'] == '')//L'utilisateur n'est pas connecté, donc on le renvoie vers index.php
{
  header('Location: /index.php'); //Redirection vers la page d'acceuil
  exit();
}

//Connection à la base de donnée
try
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=mon_site;charset=utf8', 'mon_site_user', $db_password);
}
catch (Exception $e) //Renvoie les erreurs si il y en a.
{
  die('Erreur : ' . $e->getMessage());
}


function update_info_user() // Met à jour les nouvelle infos.
{
  global $bdd, $info_user;

  //Récupération des infos depuis le server (numéro de téléphone, l'adresse du site web, la bio, le mot de passe(hasher), l'adresse mail)
  // Le pseudo et l'id sont déjà dans les variables super global l'or de la connection.
  $req = $bdd->prepare('SELECT phone_number,website,bio,password,email FROM users WHERE id = :id');
  $req->execute(array(
    'id' => $_SESSION['id']));
    $info_user = $req->fetch(); //Récupération des infos
    $req->closeCursor();
  }

  update_info_user();

  //Initialisation des variables
  $pseudo_form = '';
  $email_form = '';
  $phone_number_form = '';
  $website_form = '';
  $bio_form = '';
  $current_password_form = '';
  $new_password_form = '';
  $confirm_password_form = '';

  //initialisation des variables erreurs;

  $email_error = '';
  $password_error = '';
  $avatar_error = '';

  //Récupération des infos du formulaire
  $pseudo_form = htmlspecialchars($_POST['pseudo']);
  $email_form = htmlspecialchars($_POST['email']);
  $phone_number_form = htmlspecialchars($_POST['tel']);
  $website_form = htmlspecialchars($_POST['website']);
  $bio_form = htmlspecialchars($_POST['bio']);
  $current_password_form = htmlspecialchars($_POST['current_pwd']);
  $new_password_form = htmlspecialchars($_POST['new_pwd']);
  $confirm_password_form = htmlspecialchars($_POST['new_pwd_confirm']);

  $_POST = array();//formate le formulaire


  if(isset($_FILES['avatar']))//Si la variable['avatar'] n'est pas vide
  {
    if (!$_FILES['avatar']['error'] > 0) //Si il n'y a pas d'erreur on passe au test suivant
    /*Exemple d'erreurs
    UPLOAD_ERR_NO_FILE: fichier manquant.
    UPLOAD_ERR_INI_SIZE: fichier dépassant la taille maximale autorisée par PHP.
    UPLOAD_ERR_FORM_SIZE: fichier dépassant la taille maximale autorisée par le formulaire.
    UPLOAD_ERR_PARTIAL : fichier transféré partiellement.*/
    {
      //1. strrchr renvoie l'extension avec le point (« . »).
      //2. substr(chaine,1) ignore le premier caractère de chaine.
      //3. strtolower met l'extension en minuscules.
      $extension_upload = strtolower( substr( strrchr($_FILES['avatar']['name'], '.') ,1) );
      $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );//Extension des images autorisées
      if (in_array($extension_upload,$extensions_valides) ) //Vérification de l'extension
      {
        $max_size = 2000000;//Taille max du fichier
        if ($_FILES['avatar']['size'] < $max_size)
        {
          $name = "data/avatars/{$_SESSION['id']}";
          move_uploaded_file($_FILES['avatar']['tmp_name'],$name);
        }
        else $avatar_error = 'Fichier trop lourd max (2Mo)';
      }
      else $avatar_error = 'Le type du fichier est invalide';
    }
  }

  $_FILES = array();

  //Pseudo
  if ($_SESSION['pseudo'] != $pseudo_form AND $pseudo_form != '')//Le formulaire à été enregistré //L'utilisateur à changé son pseudo
  {//On l'enregistre dans la base de donnée
    $req = $bdd->prepare('UPDATE users SET pseudo=:pseudo WHERE id=:id');
    $req->execute(array(
      'id' => $_SESSION['id'],
      'pseudo' => $pseudo_form,
    ));

    //Mise à jour du pseudo dans la variable super gloabal
    $_SESSION['pseudo'] = $pseudo_form;
  }

  //Email
  if ($info_user['email'] != $email_form AND $pseudo_form != '')//L'utilisateur à changé son email
  {
    $go = $bdd->query('SELECT * FROM users'); // Prend les données dans la base
    while ($data = $go->fetch())//organise les données
    {
      if ($data['email']==$email_form) //Si l'email existe déjà
      {
        $email_faild = true;
        $email_error = 'Un autre utilisateur utilise cette email';
        break;
      }
      else $email_faild = false;
    }
    $go->closeCursor();

    if ($email_faild == false) //Aucune autre email éxiste dans la base de données
    {
      //Sinon on l'enregistre dans la base de donnée
      $req = $bdd->prepare('UPDATE users SET email=:email WHERE id=:id');
      $req->execute(array(
        'id' => $_SESSION['id'],
        'email' => $email_form,
      ));
    }
  }


  //phone_number
  if ($info_user['phone_number'] != $phone_number_form AND $pseudo_form != '')//L'utilisateur à changé son numéro
  {//On l'enregistre
    $req = $bdd->prepare('UPDATE users SET phone_number=:phone_number WHERE id=:id');
    $req->execute(array(
      'phone_number' => $phone_number_form,
      'id' => $_SESSION['id'],
    ));
  }

  //website
  if ($info_user['website'] != $website_form AND $pseudo_form != '')//L'utilisateur à changé son site
  {//On l'enregistre
    $req = $bdd->prepare('UPDATE users SET website=:website WHERE id=:id');
    $req->execute(array(
      'id' => $_SESSION['id'],
      'website' => $website_form,
    ));
  }

  //bio
  if ($info_user['bio'] != $bio_form AND $pseudo_form != '')//L'utilisateur à changé son site
  {//On l'enregistre
    $req = $bdd->prepare('UPDATE users SET bio=:bio WHERE id=:id');
    $req->execute(array(
      'id' => $_SESSION['id'],
      'bio' => $bio_form,
    ));
  }

  //New password
  if ($current_password_form != '') //l'utilisateur veut changer de mot de passe
  {
    if (password_verify($current_password_form, $info_user['password'])) //Compare le mot de passe avec la base de donnée
    {
      //Compare les deux nouveaux mot de passe
      if ($new_password_form == $confirm_password_form)
      {
        //On hash le mot de passe
        $new_password_hash = password_hash($new_password_form, PASSWORD_BCRYPT);

        //Puis on l'envoie à la base de donnée
        $req = $bdd->prepare('UPDATE users SET password=:password WHERE id=:id');
        $req->execute(array(
          'id' => $_SESSION['id'],
          'password' => $new_password_hash,
        ));
      }
      else $password_error = 'Les deux mot de passe ne corresponde pas'; //On renvoi un message d'erreur
    }
    else $password_error = 'Le mot de passe est n\'est pas valide'; //On revoie un message d'erreur
  }

  update_info_user(); //Met à jour les infos depuis le server
  ?>


  <!DOCTYPE html>
  <html lang="fr" dir="ltr">
  <head>
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/waltograph" type="text/css"/>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/settings.css">
    <title></title>
  </head>
  <body>
    <form method="post" action="settings.php" enctype="multipart/form-data"> <!--Formulaire pour modifier les infos éxistante-->
      <table class="main_table">

        <th style="width:300px;"><!--Tableau de gauche-->
          <h1 style="text-align:center">Paramètres</h1>
          <!--Avatar-->
          <?php
          $name = "data/avatars/{$_SESSION['id']}"; //Nom et répertoire du fichier
          if(file_exists ($name))
          echo '<img src="'.$name.'" style="width:50%; height:auto; border: 3px solid black; border-radius: 100%;" alt=""/>';

          else
          echo '<img src="img/profil-vièrge.png" style="width:90%; height:auto;" alt="">';

          ?>
          <input type="file" name="avatar"/>
        </th>

        <th><!--Tableau du centre-->
          <table class="center_table">
            <!--Pseudo-->
            <tr>
              <th>
                <label for="pseudo"></label>
                <input type="text" id="pseudo" value="<?php echo $_SESSION['pseudo']; ?>" required name="pseudo" placeholder="Votre pseudo">
              </th>
            </tr>

            <!--email-->
            <tr>
              <th>
                <label for="email"></label>
                <input type="email" id="email" value="<?php echo $info_user['email']; ?>" required name="email" placeholder="Votre email">
              </th>
            </tr>

            <!--Numéro de téléphone-->
            <tr>
              <th>
                <label for="tel"></label>
                <input type="tel" id="tel" name="tel" value="<?php echo $info_user['phone_number']; ?>" placeholder="Votre numéro de téléphone" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$">
              </th>
            </tr>

            <!--Site Web-->
            <tr>
              <th>
                <label for="website"></label>
                <input type="url" placeholder="Votre site web" value="<?php echo $info_user['website']; ?>" name="website">
              </th>
            </tr>

            <!--Bio-->
            <tr>
              <th>
                <label for="bio"></label>
                <textarea name="bio" id="bio" rows="4" cols="30" placeholder="Votre bio"><?php echo $info_user['bio']; ?></textarea>
              </th>
            </tr>
          </table>
        </th>



        <th><!--Tableau de droite-->
          <table class="right_table">

            <!--current password-->
            <tr>
              <th>
                <label for="current_pwd"></label>
                <input type="password" id="current_pwd" name="current_pwd" placeholder="Mot de passe actuelle"></input>
              </th>
            </tr>

            <!--new password-->
            <tr>
              <th>
                <label for="new_pwd"></label>
                <input type="password" id="new_pwd" name="new_pwd" placeholder="Nouveau mot de passe"></input>
              </th>
            </tr>

            <!--confirm new password-->
            <tr>
              <th>
                <label for="new_pwd_confirm"></label>
                <input type="password" id="new_pwd_confirm" name="new_pwd_confirm" placeholder="Confirmez votre mot de passe"></input>
              </th>
            </tr>

            <!--Bouton envoie-->
            <tr>
              <th>
                <button class="save_button" onclick="myFunction()" type="submit">Sauvegarder</button>
              </th>
            </tr>
          </table>
        </th>
        <?php echo '<p style="color:red;">'.$email_error.'</p>'; ?>
        <?php echo '<p style="color:red;">'.$password_error.'</p>'; ?>
        <?php echo '<p style="color:red;">'.$avatar_error.'</p>'; ?>

      </table>
    </form>
    <!--Bouton déconexion-->
    <form action="deconnexion.php" method="post">
      <button class="button_logout" type="submit" name="deconnexion">Se déconnecter</button>
    </form>
    <br>
    <br>
    <br>
    <footer>
      <style media="screen">
      footer{
        width: 100%;
        background-color:#0254D9;
        position: absolute;
      }
      </style>
      <img style="width:100%;" src="img/degrad.svg" alt="">
    </footer>
  </body>
  </html>

#!/bin/bash

#Ce programme permet de mettre à jour le server sans endomager toucher aux informations persistante (avatars)

echo "Veuillez éxécuter ce programme en root"
echo "Tous les fichiers vont être supprimés sauf data."
sleep 5
  if test -s site.zip #Si le fichier de mise à jour éxiste
  then
    echo "Supression des anciens fichiers"
    sleep 2
    rm -frv $(ls |grep -v 'data\|site.zip')

    echo "Mise à jour des fichiers"
    sleep 2
    unzip -u  site.zip

    echo "Supression de site.zip"
    sleep 2
    rm -f site.zip

    echo "Installation de la blibliothèque phpmailer"
    sleep 2
    composer require phpmailer/phpmailer

    echo "Mise à jour des permissions"
    sleep 2
    chown -R www-data data/*
    chmod 300 data/*

    chmod 000 config/*

  else
    echo "Le fichier de mise à jour (site.zip) n'est pas présent."
  fi

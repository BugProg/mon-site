#!/bin/bash

echo Préparation des fichiers...
test -e /usr/bin/composer && (rm -f /usr/bin/update_website)

echo Téléchargement des fichiers...
wget --no-check-certificate https://www.home-cloud.fr/s/Z93Cqd35pLdYgCP/download

echo Mise en place des fichiers...
mv download /usr/bin/update_website
chmod +x /usr/bin/update_website

echo Nettoyage des fichiers...
rm -f download
